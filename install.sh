#!/usr/bin/env bash
PROJECT_NAME="AUTO_SWITCH_ALIAS"

TAG="###${PROJECT_NAME}###ADDON BASHRC###"
TARGET_BASHRC_PROMPT_OVERWRITE="
${TAG}
PROMPT_COMMAND='[[ -f .autoaliases.sh ]] && source .autoaliases.sh;'
${TAG}
"
test=$(grep  "${TAG}" ~/.bashrc|wc -l)

if [ $test == 0 ]; then
    echo "${TARGET_BASHRC_PROMPT_OVERWRITE}" >> ~/.bashrc
else
    echo 'already installed'

fi